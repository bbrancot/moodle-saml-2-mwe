# Start it
* git clone https://gitlab.pasteur.fr/bbrancot/moodle-saml-2-mwe/
* cd moodle-saml-2-mwe
* docker-compose pull
* docker-compose up -d
* docker-compose logs -f

The SimpleSAML is provided by https://hub.docker.com/r/kristophjunge/test-saml-idp/

It takes some time to be up the first time, look at the logs

To reset everything : `docker-compose down --volumes`

The `SIMPLESAMLPHP_SP_ASSERTION_*` variables are found in https://0.0.0.0:8443/auth/saml2/sp/metadata.php . The values informe simplesaml on how the SP should be used.

# Configure Moodle

Download the zip for the SAML 2 plugin at https://moodle.org/plugins/auth_saml2

Connect to https://0.0.0.0:8443/

```
user:user
pwd:bitnami
```

Go to `Site administration`>`Plugins`

In `Plugins` go to `Install plugins` and install SAML2 from the zip, follow the process up to an internal restart.

Go to `Site administration`>`Plugins`>`Authentication`

Enable SAML2


# Configure SAML2 Main settings

## IdP metadata xml OR public xml URL

By the content of http://0.0.0.0:8098/simplesaml/saml2/idp/metadata.php, of even better the url itself if it works, I guess it does not as ssl certificate are not valide

http://hub-test-idp.test/simplesaml/saml2/idp/metadata.php

## Debugging

yes

## Regenerate certificate

> Needed after first configuration !

Default cert might be outdated (https://stackoverflow.com/a/61957211/2144569)

## Entity ID
```
0.0.0.0
hub-teaching-k8s.test
```

## Auto create users

True

Needed to create new user conecting to moodle. It also need the mapping !

Keep it to False If you just want to test the link between the SP and IdP, but don't want for now user to be created.


# Data mapping

To see the attribut provided by the Idp, see https://github.com/catalyst/moodle-auth_saml2#debugging and use 

```
/auth/saml2/test.php
https://hub-teaching-k8s.test/auth/saml2/test.php
```

For all, set 

| Field          | Values           |
|----------------|------------------|
| `Update local` | `On every login` |
| `Lock value`   | `Locked`         |

## Data mapping (First name)

first-name

(as in authsources.php)

## Data mapping (Surname)

last-name

## Data mapping (Email address)

email

## Data mapping (Middle name)

middle-name

Maybe set `Lock value` to `Unlock`, to see experience how it works.

# Using minikube

Install minikube and start it (cf docs?)

enable ingress:
```sh
minikube addons enable ingress
minikube addons enable ingress-dns
```
see https://minikube.sigs.k8s.io/docs/handbook/addons/ingress-dns/, I followed "Linux OS with Network Manager", plus I added to /etc/resolv.conf
```txt
search test
nameserver 192.168.49.2
timeout 5
```

```sh
sudo systemctl restart NetworkManager.service
```


```sh
minikube start
minikube dashboard &
sudo ls # to trigger pwd prompt, and be able to run minikube tunnel & without password prompt
minikube tunnel &
```

## start moodle and idp
```sh
export NO_SECRET=""
kubectl get secret my-release-moodle || NO_SECRET=$?

if [ $NO_SECRET ]; then
    export MOODLE_PASSWORD=""
    export MARIADB_ROOT_PASSWORD=""
    export MARIADB_PASSWORD=""
else
    export MOODLE_PASSWORD=$(kubectl get secret my-release-moodle -o jsonpath="{.data.moodle-password}" | base64 -d)
    export MARIADB_ROOT_PASSWORD=$(kubectl get secret my-release-mariadb -o jsonpath="{.data.mariadb-root-password}" | base64 -d)
    export MARIADB_PASSWORD=$(kubectl get secret my-release-mariadb -o jsonpath="{.data.mariadb-password}" | base64 -d)
fi

VALUES="--values ./chart/values.yaml"
if [ "$(kubectl config current-context)" != "minikube" ]; then 
  export VALUES="$VALUES --values ./chart/values.k8s.yaml"
fi

helm upgrade --install --set moodle.moodlePassword=$MOODLE_PASSWORD --set moodle.mariadb.auth.rootPassword=$MARIADB_ROOT_PASSWORD --set moodle.mariadb.auth.password=$MARIADB_PASSWORD  my-release $VALUES ./chart/ 


echo MoodleUsername: user
echo MoodlePassword: $(kubectl get secret my-release-moodle -o jsonpath="{.data.moodle-password}" | base64 -d)
```

endpoints :
 * idp is at http://hub-test-idp.test/simplesaml/saml2/idp/metadata.php
 * idp is at https://hub-test-idp.test/simplesaml/saml2/idp/metadata.php
 * moodle is at https://hub-teaching-k8s.test/

### fallback when ingress-dns does not work
```
# service-idp is available at
export IDP_IP=$(kubectl get svc my-release-idp --template='{{.spec.clusterIP}}')
echo "http://$IDP_IP:8080/simplesaml/saml2/idp/metadata.php"

# moodle is at 
export MOODLE_IP=$(kubectl get svc my-release-moodle --template='{{.spec.clusterIP}}')
echo "https://$MOODLE_IP/"

```

# Using K8S

```sh
helm unsintall my-release-to-local
helm install my-release-to-local ./chart-idp-to-local/
```
