<?php

$config = array(

    'admin' => array(
        'core:AdminPassword',
    ),

    'example-userpass' => array(
        'exampleauth:UserPass',
        'alovelace:pwd' => array(
            'uid' => 'alovelace',
            'eduPersonAffiliation' => array('group1'),
            'email' => 'ada.lovelace@pasteur.fr',
            'first-name' => 'Ada',
            'last-name' => 'Lovelace',
        ),
        'aturing:pwd' => array(
            'uid' => 'aturing',
            'eduPersonAffiliation' => array('group2'),
            'email' => 'alan.turing@pasteur.fr',
            'first-name' => 'Alan',
            'middle-name' => 'Mathison',
            'last-name' => 'Turing',
        ),
    ),

);
